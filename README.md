# Q3Rally docker

### Play the [Q3Rally](https://github.com/Q3Rally-Team/q3rally) game with docker and docker compose

![Q3Rally](game.png)

## Build

```
$ docker compose build
```

## Usage

```
$ xhost +local:debian
$ docker compose up
$ docker compose down
```

## License

	MIT
