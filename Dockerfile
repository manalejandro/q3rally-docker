FROM nvidia/opengl:base
RUN apt update && apt install -y git make gcc libsdl2-dev && apt clean
RUN git clone --depth 1 https://github.com/Q3Rally-Team/q3rally /q3rally
WORKDIR /q3rally
RUN cd engine && ./make-linux-portable.sh x86_64
ENTRYPOINT ["./run.sh"]
